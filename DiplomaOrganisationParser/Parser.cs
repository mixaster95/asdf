﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiplomaOrganisationParser
{
    static class Parser
    {
        
        static string[] organisationSeparators = { "and", " and", ";" };

        static string[] organisationKeyWords = { "blood service", "clinic", "school", "laboratory", "ecole", "hospital", "academy", "college", "institut", "institute", "instituto", "istituto", "alliance", "ministry", "gmbh3", "center", "centre", "centro", "inc", "ltd", "corporation", "univ", "hochschule", "universitat", "university" };

        static string[] deleteKeyWords = { "road", "marg", "post box", "allee", "campus", "street", "boulevard", "province", "park", "district", "area", "avda", "sector", "extension",  "division", "faculty", "department", "plateforme" };

        public static void ProcessOrganisations(string folderPath)
        {
            var resultList = new List<Organisation>();

            var rawOrganisations = File.ReadAllLines(folderPath + "TestList2.txt");
            var countries = File.ReadAllLines(folderPath + "countries.txt");

            //var separatedOrganisations = OrganisationsSeparatedByCountry(rawOrganisations, countries);
            //File.WriteAllLines(folderPath + "separatedOrganisations.txt", OrganisationsSeparatedByCountry(rawOrganisations, countries));
            var separatedOrganisations = File.ReadAllLines(folderPath + "separatedOrganisations.txt");
            var counter = 1;

            foreach (var organisation in separatedOrganisations)
            {
                Console.WriteLine("Processing...{0} of {1}", counter++, separatedOrganisations.Length);

                var result = new Organisation();
                var preparedString = organisation.Replace(";", "").Replace(".", "")/*.Replace(" ", "")*/.Split(',');

                result.Id = int.Parse(preparedString[0]);
                for (int i = 0; i < preparedString.Length; i++)
                {
                    foreach (var country in countries)
                    {
                        if (preparedString[i].Contains(country))
                        {
                            result.Country = country;
                            break;
                        }
                    }
                }
                for (int i = 0; i < preparedString.Length; i++)
                {
                    foreach (var keyWord in organisationKeyWords)
                    {
                        if (preparedString[i].Contains(keyWord))
                        {
                            result.Name = preparedString[i];
                            if (keyWord == "university")
                            {
                                goto A;
                            }
                            //break;
                        }
                    }
                }
                A:
                resultList.Add(result);
            }
            File.WriteAllText(folderPath + "result.txt", JsonConvert.SerializeObject(resultList, Formatting.Indented));
        }

        public static void ProcessOrganisations2(string folderPath)
        {
            var resultList = new List<Organisation>();
            var stringResultList = new List<string>();

            var rawOrganisations = File.ReadAllLines(folderPath + "PubMed.txt");
            var countries = File.ReadAllLines(folderPath + "countries.txt");
            var countriesAndCities = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(File.ReadAllText(folderPath + "Counties&Cities.txt").ToLower());

            var separatedOrganisations = OrganisationsSeparatedByCountry(rawOrganisations, countries).ToArray();
            //var separatedOrganisations = File.ReadAllLines(folderPath + "separatedOrganisations.txt");
            var counter = 1;

            foreach (var organisation in separatedOrganisations)
            {
                Console.WriteLine("Processing...{0} of {1}", counter++, separatedOrganisations.Length);
                var result = new Organisation();
                var stringResult = "";

                var preparedString = organisation.Replace(";", ".").Split(',').ToList();

                result.Id = int.Parse(preparedString[0]);
                preparedString.RemoveAt(0);

                for (int i = 0; i < preparedString.Count; i++)
                {
                    var index1 = preparedString[i].IndexOf("electronic address:");
                    if (index1 != -1)
                    {
                        preparedString[i] = preparedString[i].Remove(index1, preparedString[i].Length - index1);
                    }
                }              

                var countryForCity = "";
                foreach (var country in countries)
                {
                    if (preparedString.Count() > 0)
                    {
                        if (preparedString[preparedString.Count() - 1].Replace(" ","").Contains(country + ".") || preparedString[preparedString.Count() - 1].Trim() == country)
                        {
                            preparedString.RemoveAt(preparedString.Count() - 1);

                            if (country == "uk")
                            {
                                countryForCity = "united kingdom";
                            }
                            if (country == "korea")
                            {
                                countryForCity = "republic of korea";
                            }
                            else countryForCity = country;
                            break;
                        }
                    }
                }

                var pattern = @"([\w\.\-]+)@([\w\-]+)(\.\w+)(\.\w+)*";
                if (preparedString.Count() > 0)
                {
                    if (Regex.IsMatch(preparedString[preparedString.Count() - 1], pattern))
                    {
                        foreach (var country in countries)
                        {
                            if (preparedString[preparedString.Count() - 1].Replace(" ", "").Contains(country))
                            {
                                if (country == "uk")
                                {
                                    countryForCity = "united kingdom";
                                }
                                if (country == "korea")
                                {
                                    countryForCity = "republic of korea";
                                }
                                else countryForCity = country;
                                break;
                            }
                        }
                        preparedString.RemoveAt(preparedString.Count() - 1);
                    }
                }

                for (int i = 0; i < preparedString.Count(); i++)
                {
                    var numCounter = 0;
                    foreach (var sign in preparedString[i])
                    {
                        if (char.IsDigit(sign) || char.IsWhiteSpace(sign))
                        {
                            numCounter++;
                        }
                    }
                    if (numCounter == preparedString[i].Length)
                    {
                        preparedString.RemoveAt(i);
                        i--;
                        continue;
                    }
                    var digitCounter = 0;
                    foreach (var sign in preparedString[i])
                    {
                        if (char.IsDigit(sign))
                        {
                            digitCounter++;
                        }
                        if (digitCounter > 2)
                        {
                            preparedString.RemoveAt(i);
                            i--;
                            break;
                        }
                    }
                }

                for (int i = 0; i < preparedString.Count(); i++)
                {
                    try
                    {
                        foreach (var city in countriesAndCities[countryForCity])
                        {
                            if (preparedString[i].Trim() == city)
                            {
                                for (int j = i; j < preparedString.Count(); j++)
                                {
                                    preparedString.RemoveAt(j);
                                    j--;
                                }                                
                                break;
                            }
                            var index = preparedString[i].IndexOf(city);
                            if (index != -1)
                            {
                                for (int j = index + city.Length; j < preparedString[i].Length; j++)
                                {
                                    if (char.IsDigit(preparedString[i][j]))
                                    {
                                        preparedString.RemoveAt(i);
                                        i--;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    catch
                    {
                        foreach (var city in countriesAndCities["usa"])
                        {
                            if (preparedString[i].Trim() == city)
                            {
                                for (int j = i; j < preparedString.Count(); j++)
                                {
                                    preparedString.RemoveAt(j);
                                    j--;
                                }
                                break;
                            }
                            var index = preparedString[i].IndexOf(city);
                            if (index != -1)
                            {
                                for (int j = index + city.Length; j < preparedString[i].Length; j++)
                                {
                                    if (char.IsDigit(preparedString[i][j]))
                                    {
                                        preparedString.RemoveAt(i);
                                        i--;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                if (preparedString.Count() > 1)
                {
                    foreach (var delete in deleteKeyWords)
                    {
                        for (int i = 0; i < preparedString.Count(); i++)
                        {
                            if (preparedString[i].Contains(delete))
                            {
                                preparedString.RemoveAt(i);
                                i--;
                            }
                            if (preparedString.Count() == 1) break;
                        }
                    }
                }


                if (preparedString.Count() > 0)
                {
                    for (int i = 0; i < preparedString.Count() - 1; i++)
                    {
                        stringResult += preparedString[i].Trim() + ",";
                    }
                    stringResult += preparedString[preparedString.Count() - 1].Trim();
                }
                result.Country = countryForCity;
                result.Name = stringResult;
                resultList.Add(result);
                //stringResultList.Add(stringResult);
            }

            File.WriteAllText(folderPath + "BigResult.txt", JsonConvert.SerializeObject(resultList, Formatting.Indented));
            //File.WriteAllLines(folderPath + "result2.txt", stringResultList);
            //File.WriteAllText(folderPath + "result2.txt", JsonConvert.SerializeObject(resultList, Formatting.Indented));
        }

        private static List<string> OrganisationsSeparatedByCountry(string[] organisationStrings, string[] countries)
        {
            var result = new List<string>();
            var i = 1;
            var containsCountry = false;
            foreach (var orgString in organisationStrings)
            {
                var lowerOrgString = orgString.ToLower();

                foreach (var country in countries)
                {                 
                    var separators = CountryWithSeparators(country, organisationSeparators);
                    foreach (var separator in separators)
                    {
                        while (true)
                        {
                            var index = lowerOrgString.IndexOf(separator);
                            if (index != -1)
                            {
                                result.Add(i + "," + lowerOrgString.Substring(0, index + separator.Length));
                                lowerOrgString = lowerOrgString.Remove(0, index + separator.Length);
                                containsCountry = true;
                            }
                            else break;
                        }
                    }
                    if (lowerOrgString.Contains(country + ".") || lowerOrgString.Contains(country + " ."))
                    {
                        result.Add(i + "," + lowerOrgString);
                        containsCountry = true;
                        break;
                    }
                }
                if (!containsCountry)
                {
                    result.Add(i + "," + lowerOrgString);                 
                }
                containsCountry = false;

                Console.WriteLine("Processing...{0} of {1}", i++, organisationStrings.Length);
            }
            return result;
        }

        private static List<string> CountryWithSeparators(string country, string[] separators)
        {
            var result = new List<string>();

            foreach (var separator in separators)
            {
                result.Add(", " + country + separator);
                result.Add(",  " + country + separator);
            }

            return result;
        }
    }
}
